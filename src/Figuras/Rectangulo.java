package Figuras;

public class Rectangulo extends Figura {
	//inferiorIzquierdo se hereda de Figura;
		private Punto2D superiorDerecho;

		
		public Rectangulo(Punto2D inferiorIzquierdo, Punto2D superiorDerecho) {
			super(inferiorIzquierdo);
			this.superiorDerecho = superiorDerecho;
		}

		@Override
		public void desplazar(double deltaX, double deltaY) {
			this.p1.desplazar(deltaX, deltaY);
			this.superiorDerecho.desplazar(deltaX, deltaY);

		}

		@Override
		public double calcularArea() {
			return Math.abs((this.superiorDerecho.getX() - this.p1.getX())
					* (this.superiorDerecho.getY() - this.p1.getY()));
		}

		@Override
		public String toString() {
			return "Rectangulo [superiorDerecho=" + superiorDerecho + ", inferiorIzquierdo=" + p1 + ", calcularArea()=" + calcularArea()
					+ "]";
		}

}
