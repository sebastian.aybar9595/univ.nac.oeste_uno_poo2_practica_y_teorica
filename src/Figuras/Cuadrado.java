package Figuras;

public class Cuadrado extends Rectangulo{
	
	public Cuadrado(Punto2D p1, double lado) {
		super(p1,new Punto2D(p1.getX()+lado,p1.getY()+lado) );
		
	}


}
