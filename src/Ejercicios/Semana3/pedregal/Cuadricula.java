package Ejercicios.Semana3.pedregal;

public class Cuadricula {
	
	private Punto2D posicion;
	
	private EstadoCuadricula estadoCuadricula;

	public Cuadricula(Punto2D posicion, EstadoCuadricula estadoCuadricula) {
		super();
		this.posicion = posicion;
		this.estadoCuadricula = estadoCuadricula;
	}

	public Punto2D getPosicion() {
		return posicion;
	}

	public void setPosicion(Punto2D posicion) {
		this.posicion = posicion;
	}

	public EstadoCuadricula getEstadoCuadricula() {
		return estadoCuadricula;
	}

	public void setEstadoCuadricula(EstadoCuadricula estadoCuadricula) {
		this.estadoCuadricula = estadoCuadricula;
	}
	
	

	
	
}
