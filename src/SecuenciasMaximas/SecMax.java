package SecuenciasMaximas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class SecMax {
	
	 private Integer numeroDeLineas;
	 private int [] valoresDeEntrada=null;
	 private int valoresValidos=0;
	 
	
	//leer archivo
	public  void leerArchivo() throws NumberFormatException, IOException {
	
		File f = new File("Archivos/secmax.in");
    	
		Scanner entrada = new Scanner(f);
 	    
 	    
 	  this.numeroDeLineas= entrada.nextInt();
 	    
 	   this.valoresDeEntrada=new int [numeroDeLineas];
 	    
 	    
 	    int i=0;
		while(entrada.hasNextInt()) {
			valoresDeEntrada[i]=entrada.nextInt();
			i++;
 	      }
		
		entrada.close();
	
		
	}
	
	//procesar/*
	
	public boolean esValido(int numero) {
		boolean valor=false;
		if (numero%2==0) {
			valor=false;
		}else if (numero%5==0) {
			valor=false;
		}else if(numero%3==0) {
			valor=false;
		}else {valor=true;}
	
		return valor;
	
	}
	
	
	public  void secuenciaMaximaMetodo() {
		
		int contLongitud=0;
		int longitudMaxima=0;
		ArrayList <Integer> longitudDeSecuencias=new ArrayList<Integer>();
	
		
		
		for(int tmp: this.valoresDeEntrada) {
			if(esValido(tmp)) {
				this.valoresValidos++;
			}	}
		
		for(int tmp: this.valoresDeEntrada) {
			if(esValido(tmp)) {
				contLongitud++;
			}else {
				longitudMaxima=contLongitud;
				longitudDeSecuencias.add(longitudMaxima);
				
				contLongitud=0;}
			
		}
		
		int max = 0;
        for (int i = 0; i < longitudDeSecuencias.size(); i++) {
            if (longitudDeSecuencias.get(i) > max ||longitudDeSecuencias.get(i)==max ) {
            	max =longitudDeSecuencias.get(i);
                
            }
        }
		
		
        crearArchivosSalida(valoresValidos, max);
	
		
	}
	
	
	

	
	//salida
	public static void crearArchivosSalida(int cantidadDeValoresValidos, int longitudDeSecuencia) {
	FileWriter flwriter = null;
	try {
		//crea el flujo para escribir en el archivo
		
		flwriter = new FileWriter("Archivos/secmax.out");
		
		//crea un buffer o flujo intermedio 
		
		BufferedWriter bfwriter = new BufferedWriter(flwriter);
		
		bfwriter.write(cantidadDeValoresValidos+"\n"+longitudDeSecuencia+" "+"\n");
		
	
		//cierra el buffer intermedio
		bfwriter.close();

	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (flwriter != null) {
			try {//cierra el flujo principal
				flwriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}}
	



	
	public static void main(String[] args) throws NumberFormatException, IOException {
		SecMax sm=new SecMax();
		
		sm.leerArchivo();
		
		
		sm.secuenciaMaximaMetodo();
		
	}

}

