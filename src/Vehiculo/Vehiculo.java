package Vehiculo;

public abstract class Vehiculo {
	private double kmRecorridos;
	private Persona chofer;
	public abstract void cambiarChofer(Persona chofer);
	
	public void asignarChofer(Persona chofer) {
		this.chofer = chofer;
	}
	
	public void asignarKmRecorridos(double km) {
		this.kmRecorridos = km;
	}
	
	public double obtenerKmRecorridos() {
		return kmRecorridos;
	}
	
	public Persona obtenerPersona() {
		return chofer;
	}
	
	public Vehiculo(double km, Persona nombre) {
		this.chofer = nombre;
		this.kmRecorridos = km;
	}
		
}
