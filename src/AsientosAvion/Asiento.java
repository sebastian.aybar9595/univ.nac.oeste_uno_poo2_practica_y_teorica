package AsientosAvion;

public class Asiento {
	
	private int aNumero;
	private EstadoAsiento eAsiento;
	private Pasajero pasajero;
	
	public Asiento( int aNumero, EstadoAsiento eAsiento )	{
	   aNumero=aNumero;
	   this.seteAsiento(eAsiento);
	   pasajero = null;
	   
	}

	public void seteAsiento(EstadoAsiento eAsiento) {
		this.eAsiento = eAsiento;
	}

	public int getaNumero() {
		return aNumero;
	}


	public EstadoAsiento geteAsiento() {
		return eAsiento;
	}

	public Pasajero getPasajero() {
		return pasajero;
	}

	public void setPasajero(Pasajero pasajero) {
		this.pasajero = pasajero;
	}

	
	
	
	

}
