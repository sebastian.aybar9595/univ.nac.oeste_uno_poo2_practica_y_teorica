package DepositoSubterraneo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

public class DepositoSubterraneo {

	private int numeroDeLineas;
	private double volumenArecibir;
	private double capacidadTotalDepositos=0;
	private ArrayList <DepositoPetroleo>arrayDepositos;
	private int i;
	private double acuCapacidad=0;
	private int cantidadDepositos;
	private int profundidadDePiso;
	private double resultado;
	
	//leer archivo
		public  void leerArchivo() throws NumberFormatException, IOException {
		
			File f = new File("Archivos/depositos.in");
	    	
			Scanner entrada = new Scanner(f);
	 	    
	 	//leemos primera linea con cantidad de depositos    
	 	  this.numeroDeLineas= entrada.nextInt();
	 	  
	 	//creamos un arrayList<DepositoPetroleo> para llenarlo con los datos del archivo de entrada
	 	    
	 	arrayDepositos= new ArrayList <DepositoPetroleo>();
	 	    
	 //leemos el archivo y creamos objetos DepositoPetroleo para poner en nuestro array
	 	
	 	int i;
		for(i=0;i<numeroDeLineas;i++) {
			
			arrayDepositos.add(new DepositoPetroleo(entrada.nextInt(),entrada.nextInt()));
			
		}
	 	
		
		//leemos la ultima linea con el volumen de petroleo a almacenar
		
		this.volumenArecibir=entrada.nextDouble();
						
			entrada.close();
		
			
		}
		
		
	//proceso
		
	public  void procesarDeposito() {
		
		//calculamos la capacidad total de nuestros depositos
for(DepositoPetroleo tmp: arrayDepositos) {
			
	this.capacidadTotalDepositos=this.capacidadTotalDepositos+tmp.getCapacidadcubica();}
//calculamos si podemos almacenar el caudal y de no ser asi realizamos una salida

if(volumenArecibir>capacidadTotalDepositos) { 
	
	resultado=volumenArecibir-capacidadTotalDepositos;
	int rebalsa= (int) Math.floor(this.resultado);
	
	crearArchivoSalida(rebalsa);
	
	//System.out.println(rebalsa);
}else {
	
	//resolvemos segunda salida
	
	while(acuCapacidad<volumenArecibir) {
	for(i=0;i<arrayDepositos.size();i++) {
		this.acuCapacidad=this.acuCapacidad+arrayDepositos.get(i).getCapacidadcubica();
		
	}
		this.cantidadDepositos=i;
	}
	
	//this.profundidadDePiso=arrayDepositos.get(0).getProfundidad()-arrayDepositos.get(cantidadDepositos-1).getProfundidad();
	//calculamos volumen sobrante en los tanques llenos
	double volumenSobrante=(volumenArecibir-capacidadTotalDepositos)/cantidadDepositos;
	double profundidad=volumenSobrante/arrayDepositos.get(0).getSuperficieBase();
	
	profundidad=Math.round(profundidad)*-1;
	/*System.out.println(volumenSobrante);

	System.out.println(Math.round(profundidad)*-1);*/
	crearArchivoSalida2(cantidadDepositos,Math.floor(profundidad));
}
		
	}
	


	//salida posible 1
	private static  void crearArchivoSalida(int cantRebalsa) {
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			
			flwriter = new FileWriter("Archivos/depositos.out");
			
			//crea un buffer o flujo intermedio 
			
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
		
			bfwriter.write("REBASAN: "+" "+cantRebalsa+"\n");
	
			//cierra el buffer intermedio
			bfwriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
}
	
	private static  void crearArchivoSalida2(int cDepositos, double profundidad) {
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			
			flwriter = new FileWriter("Archivos/depositos.out");
			
			//crea un buffer o flujo intermedio 
			
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
		
			bfwriter.write(cDepositos+"\n");
			bfwriter.write(profundidad+"\n");
	
			//cierra el buffer intermedio
			bfwriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
}
	public static void main(String[] args) throws NumberFormatException, IOException {
		DepositoSubterraneo DS= new DepositoSubterraneo();
		
		DS.leerArchivo();
		DS.procesarDeposito();
	}
}
